# master-os

Repository teaching you everything you need about basic Unix usage and navigation, as well as some OSX specific ease of life stuff.

## Unix environment 

[Getting started with a unix system](http://www.linuxcommand.org/)

[More verbose tutorial](https://www.tutorialspoint.com/unix/index.htm)

[Basic set of commands](http://www.makeuseof.com/tag/an-a-z-of-linux-40-essential-commands-you-should-know/)

[Command cheatsheet](http://ryanstutorials.net/linuxtutorial/cheatsheet.php)

[Scheduling jobs with Cron](http://www.yourownlinux.com/2014/04/schedule-your-jobs-in-linux-with-cron-examples-and-tutorial.html)

[Unix Processes](http://www.doc.ic.ac.uk/~wjk/UnixIntro/Lecture4.html)

[Processes exercises](http://www.doc.ic.ac.uk/~wjk/UnixIntro/Exercise4.html)

[Bash profile aliases](https://www.cyberciti.biz/tips/bash-aliases-mac-centos-linux-unix.html)

[About Screen](https://www.rackaid.com/blog/linux-screen-tutorial-and-how-to/) 

[More about Screen](http://www.tecmint.com/screen-command-examples-to-manage-linux-terminals/)

[SSH Keys](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2) (as an excrecise before going further - create an SSH key and 
use it to access something, e.g. add your key to your GitLab account and use it to clone a repository)

[Above and beyond - extremely in-depth Linux "cookbook"](http://www.dsl.org/cookbook/cookbook_toc.html)

## Bash programming:

[Basic scripts](http://www.learnshell.org/)

[In-depth bash scripting tutorial](http://tldp.org/LDP/abs/html/)

## OSX specifics

### Iterm2

[Iterm2 - powerful terminal replacement for OSX](https://www.iterm2.com/version3.html)

[itermocil - ability to open project with subcommands](https://github.com/PythonicNinja/itermocil)

[Basic iterm configuration](http://pythonic.ninja/configuration-for-productive-terminal-on-osx.html)

[Heavily configured bash profile, with multiple usefull commands and aliases.](https://gitlab.com/limebrains/bash_profile/blob/master/README.md)

### HomeBrew package manager

https://computers.tutsplus.com/tutorials/homebrew-demystified-os-xs-ultimate-package-manager--mac-44884

### Useful apps:

[cp-clip - clipboard manager](https://github.com/aklein13/cp-clip)

## CLI project management tool
[pet - Our open-source project](https://github.com/limebrains/pet/)

